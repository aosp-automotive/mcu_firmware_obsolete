/*
   eeprom memory layout;
   1B [A,D] | 1B pin# | 2B HID keycode | 2B ADC (analog only)

   All pins, including ANALOG, are set to activate internal pull-up resistors.
   In the case of the analog pins, this is to ensure readings above 900 when
   nothing is connected. Value of internal pull-up will be somewhere between
   30-35 kohm. Since SWI requires external pull up of around 1.6 kohm, this is
   ok and won't do anything besides a minor skew of the values, which are
   learned anyway.

   1/(1/30 + 1/1.6) = 1.52 kohm.


   Note: https://source.android.com/devices/input/keyboard-devices#hid-consumer-page-0x0c
   ** use HID usage code 2-bytes

   Codes:
   VOL+  0x00e9 (233)           HOME    0x0223 (547)      BACK 0x0224 (548)
   VOL-  0x00ea (234)           MENU    0x0040 (64)
   CHAN+ 0x009c (156)           BRIGHT+ 0x006f (111)
   CHAN- 0x009d (157)           BRIGHT- 0x0070 (112
   MUTE  0x00e2 (226)           RESET   0xffff (65535)

   WATCHDOG:
   The function of the watchdog timer is extremely simple. It uses a single variable
   'unsigned long watchdog'. If the value of watchdog is 0, it is not enabled. If it is
   set to any value greater than 0, then that is to be taken as the millis() as of the
   last time a bark is received. If the value of watchdog is ever more than 15 seconds
   older than millis(), the reset pin is toggled on for 250 ms. Reception of the disable
   command sets watchdog=0;

   RESET:
   Added reset capability. This is the closest that is available to a "reboot-bootloader"
   function. The "bootloader" firmware will only boot to "bootloader mode" when one of
   two conditions are met; (a) the reset button is pressed quickly after coming out of
   reset, and (b) the application storage area is blank. If there is no access to the
   reset pin, then (b) is the only option, and indeed, is the approach taken with the
   reset mechanism whereby the CDC port is opened at 1200 bps. Since it can be nice to
   eliminate the otherwise unnecessary CDC port, this function has now been tied to the
   hidraw port that is otherwise used for setting configurations. Once in bootloader
   mode, the application area can be reprogrammed using bossac.
*/

#include "Arduino.h"
#include <HID-Settings.h>
#include <HID-Project.h>
#include <FlashAsEEPROM.h>
#include "wiring_private.h"
#include "Reset.h"

#define PIN_OUT_14V           0
#define PIN_OUT_5V            1
#define PIN_OUT_AMP_UNMUTE    2
#define PIN_OUT_AMP_ENABLE    3
#define PIN_IN_ACC            4
#define PIN_IN_LIGHT          5
#define PIN_IN_REVERSE        6
#define PIN_PWM_FAN           7 // PA8
#define PIN_PWM_BACKLIGHT     8 // PA16
#define PIN_IN_SWI0           14
#define PIN_IN_SWI1           15
#define PIN_OUT_REG           24
#define PIN_RST               23 // PA19

#define PWM_PRESCALER         5
#define PWM_RESOLUTION        255

/*  An ADC reading above SWI_THRESH is considered "all buttons are released / off"
    An ADC reading within SWI_HYST of a stored value is considered ON for that button.
*/
#define SWI_THRESH            900
#define SWI_HYST              10

#define WATCHDOG_TIMEOUT_MS   15000

/*
   Available pins;
   12 (A5)
   13 (in use by LED, but easily freed)
   16 (A2)
   17 (A3)
   18 (A4)
   21 (D)
   22 (D)
   23 (D)
   24 (D)
*/

struct analogInput {
  uint8_t entries; // also serves as "enabled".
  int16_t down; // index of keycode and value of key that is down.
  uint16_t keycode[32];
  uint16_t value[32];
  uint16_t lastreading;
};
struct analogInput adc[2];

uint8_t loopcounter = 0;
uint32_t acccounter = 0;
bool accon = true;
uint16_t startup_delay = 0;
uint32_t watchdog = 0;
bool serialdbg = false;

uint8_t serData[255];
uint16_t serAvail;

uint8_t hidcmd;
bool prog_mode = false;
uint16_t swi_val[2];
uint8_t swi_send[5];
uint8_t ack[2];
uint16_t addy;

// Wait for synchronization of registers between the clock domains
static void syncTC_8(Tc* TCx) {
  while (TCx->COUNT8.STATUS.bit.SYNCBUSY);
}

// Wait for synchronization of registers between the clock domains
static void syncTCC(Tcc* TCCx) {
  while (TCCx->SYNCBUSY.reg & TCC_SYNCBUSY_MASK);
}

/*
   prescaler should be set to one of the following;
   0 = DIV1 -- 187.5 kHz at res = 255
   1 = DIV2 -- 93.76 kHz
   2 = DIV4 -- 44.88 kHz
   3 = DIV8 -- 23.44 kHz
   4 = DIV16 -- 11.72 kHz
   5 = DIV64 -- 2.93 kHz
   6 = DIV256 -- 732 Hz (default for analogWrite(..))
   7 = DIV1024 -- 183 Hz

   res 1-255 allows further control of frequency at the cost of resolution.
   res = 127 --> freq = 2* what it would be with res = 255.
   Pick highest prescaler that is NOT higher than target freq, then fine tune res.

   val = [0..255] regardless of resolution
*/
void analogWriteDiv(uint32_t pin, uint32_t val, uint8_t prescaler, uint32_t res) {
  PinDescription pinDesc = g_APinDescription[pin];
  uint32_t attr = pinDesc.ulPinAttribute;
  uint32_t value = val * res / 0xff;

  if ((attr & PIN_ATTR_PWM) == PIN_ATTR_PWM) {

    uint32_t tcNum = GetTCNumber(pinDesc.ulPWMChannel);
    uint8_t tcChannel = GetTCChannelNumber(pinDesc.ulPWMChannel);
    static bool tcEnabled[TCC_INST_NUM + TC_INST_NUM];

    if (attr & PIN_ATTR_TIMER) {
#if !(ARDUINO_SAMD_VARIANT_COMPLIANCE >= 10603)
      // Compatibility for cores based on SAMD core <=1.6.2
      if (pinDesc.ulPinType == PIO_TIMER_ALT) {
        pinPeripheral(pin, PIO_TIMER_ALT);
      } else
#endif
      {
        pinPeripheral(pin, PIO_TIMER);
      }
    } else if ((attr & PIN_ATTR_TIMER_ALT) == PIN_ATTR_TIMER_ALT) {
      //this is on an alt timer
      pinPeripheral(pin, PIO_TIMER_ALT);
    } else {
      return;
    }

    if (!tcEnabled[tcNum]) {
      tcEnabled[tcNum] = true;

      uint16_t GCLK_CLKCTRL_IDs[] = {
        GCLK_CLKCTRL_ID(GCM_TCC0_TCC1), // TCC0
        GCLK_CLKCTRL_ID(GCM_TCC0_TCC1), // TCC1
        GCLK_CLKCTRL_ID(GCM_TCC2_TC3),  // TCC2
        GCLK_CLKCTRL_ID(GCM_TCC2_TC3),  // TC3
        GCLK_CLKCTRL_ID(GCM_TC4_TC5),   // TC4
        GCLK_CLKCTRL_ID(GCM_TC4_TC5),   // TC5
        GCLK_CLKCTRL_ID(GCM_TC6_TC7),   // TC6
        GCLK_CLKCTRL_ID(GCM_TC6_TC7),   // TC7
      };
      uint16_t TC_DIV_IDs[] = {
        TC_CTRLA_PRESCALER_DIV1,
        TC_CTRLA_PRESCALER_DIV2,
        TC_CTRLA_PRESCALER_DIV4,
        TC_CTRLA_PRESCALER_DIV8,
        TC_CTRLA_PRESCALER_DIV16,
        TC_CTRLA_PRESCALER_DIV64,
        TC_CTRLA_PRESCALER_DIV256,
        TC_CTRLA_PRESCALER_DIV1024
      };
      uint16_t TCC_DIV_IDs[] = {
        TCC_CTRLA_PRESCALER_DIV1,
        TCC_CTRLA_PRESCALER_DIV2,
        TCC_CTRLA_PRESCALER_DIV4,
        TCC_CTRLA_PRESCALER_DIV8,
        TCC_CTRLA_PRESCALER_DIV16,
        TCC_CTRLA_PRESCALER_DIV64,
        TCC_CTRLA_PRESCALER_DIV256,
        TCC_CTRLA_PRESCALER_DIV1024
      };
      GCLK->CLKCTRL.reg = (uint16_t) (GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_IDs[tcNum]);
      while (GCLK->STATUS.bit.SYNCBUSY == 1);

      // Set PORT
      if (tcNum >= TCC_INST_NUM) {
        // -- Configure TC
        Tc* TCx = (Tc*) GetTC(pinDesc.ulPWMChannel);
        // Disable TCx
        TCx->COUNT8.CTRLA.bit.ENABLE = 0;
        syncTC_8(TCx);
        // Set Timer counter Mode to 8 bits, normal PWM, prescaler 1/256
        TCx->COUNT8.CTRLA.reg |= TC_CTRLA_MODE_COUNT8 | TC_CTRLA_WAVEGEN_NPWM | TC_DIV_IDs[prescaler];
        syncTC_8(TCx);
        // Set the initial value
        TCx->COUNT8.CC[tcChannel].reg = (uint8_t) value;
        syncTC_8(TCx);
        // Set PER to maximum counter value (res)
        TCx->COUNT8.PER.reg = res;
        syncTC_8(TCx);
        // Enable TCx
        TCx->COUNT8.CTRLA.bit.ENABLE = 1;
        syncTC_8(TCx);
      } else {
        // -- Configure TCC
        Tcc* TCCx = (Tcc*) GetTC(pinDesc.ulPWMChannel);
        // Disable TCCx
        TCCx->CTRLA.bit.ENABLE = 0;
        syncTCC(TCCx);
        // Set prescaler
        TCCx->CTRLA.reg |= TCC_DIV_IDs[prescaler];
        syncTCC(TCCx);
        // Set TCx as normal PWM
        TCCx->WAVE.reg |= TCC_WAVE_WAVEGEN_NPWM;
        syncTCC(TCCx);
        // Set the initial value
        TCCx->CC[tcChannel].reg = (uint32_t) value;
        syncTCC(TCCx);
        // Set PER to maximum counter value (res)
        TCCx->PER.reg = res;
        syncTCC(TCCx);
        // Enable TCCx
        TCCx->CTRLA.bit.ENABLE = 1;
        syncTCC(TCCx);
      }
    } else {
      if (tcNum >= TCC_INST_NUM) {
        Tc* TCx = (Tc*) GetTC(pinDesc.ulPWMChannel);
        TCx->COUNT8.CC[tcChannel].reg = (uint8_t) value;
        syncTC_8(TCx);
      } else {
        Tcc* TCCx = (Tcc*) GetTC(pinDesc.ulPWMChannel);
        TCCx->CTRLBSET.bit.LUPD = 1;
        syncTCC(TCCx);
        TCCx->CCB[tcChannel].reg = (uint32_t) value;
        syncTCC(TCCx);
        TCCx->CTRLBCLR.bit.LUPD = 1;
        syncTCC(TCCx);
      }
    }
  }
}

void load() {
  // initialize adc structures
  for (uint8_t i = 0; i < 2; i++) {
    adc[i].entries = 0;
    adc[i].down = -1; // because this is an index, values must start at 0. Negative means UP.
    for (uint8_t j = 0; j < 32; j++) {
      adc[i].keycode[j] = 0;
      adc[i].value[j] = 0;
    }
    adc[i].lastreading = 0x03ff;
  }

  // load key configuration from eeprom
  if (EEPROM.isValid() && EEPROM.length() > 0) {
    for (uint16_t i = 0; i < EEPROM.length();) {
      uint8_t type = EEPROM.read(i);
      uint8_t pin = EEPROM.read(i + 1);

      if (type == 'A' && (pin == 0 || pin == 1) && adc[pin].entries < 32) {
        adc[pin].keycode[adc[pin].entries] = EEPROM.read(i + 2) * 256 + EEPROM.read(i + 3);
        adc[pin].value[adc[pin].entries] = EEPROM.read(i + 4) * 256 + EEPROM.read(i + 5);

        adc[pin].entries++;

        i += 6;
      } else break;
    }
  } else {
    // If the eeprom hasn't been programmed yet, set vol+/- keys.
    // Note that the "value" entries are set to 0, so the adc pins
    // have to be pulled all the way down to 0 in order to trigger.
/*
    adc[0].keycode[adc[0].entries] = 0xe9;
    adc[0].value[adc[0].entries] = 0x0;
    adc[0].entries++;

    adc[1].keycode[adc[1].entries] = 0xea;
    adc[1].value[adc[1].entries] = 0x0;
    adc[1].entries++;*/

    // vol+
    adc[1].keycode[adc[1].entries] = 0xe9;
    adc[1].value[adc[1].entries] = 0x204;
    adc[1].entries++;

    // vol-
    adc[1].keycode[adc[1].entries] = 0xea;
    adc[1].value[adc[1].entries] = 0x30a;
    adc[1].entries++;

    // mode
    adc[0].keycode[adc[0].entries] = 0x224;
    adc[0].value[adc[0].entries] = 0x0;
    adc[0].entries++;

    // offhook
    adc[0].keycode[adc[0].entries] = 0x223;
    adc[0].value[adc[0].entries] = 0x204;
    adc[0].entries++;

    // up
    adc[1].keycode[adc[1].entries] = 0x6f;
    adc[1].value[adc[1].entries] = 0x0;
    adc[1].entries++;

    // down
    adc[1].keycode[adc[1].entries] = 0x70;
    adc[1].value[adc[1].entries] = 0xfe;
    adc[1].entries++;

    // screaming
    adc[0].keycode[adc[0].entries] = 0xe2;
    adc[0].value[adc[0].entries] = 0x30c;
    adc[0].entries++;

    // onhook
    adc[0].keycode[adc[0].entries] = 0xffff;
    adc[0].value[adc[0].entries] = 0xff;
    adc[0].entries++;
  }
}

void setup() {
  // Turn on 3v3 regulator
  pinMode(PIN_OUT_REG, OUTPUT);
  digitalWrite(PIN_OUT_REG, HIGH);

  // Set up the RESET signal
  pinMode(PIN_RST, OUTPUT);
  digitalWrite(PIN_RST, LOW);
  
  // Turn on power outputs
  pinMode(PIN_OUT_14V, OUTPUT);
  digitalWrite(PIN_OUT_14V, HIGH);
  pinMode(PIN_OUT_5V, OUTPUT);
  digitalWrite(PIN_OUT_5V, HIGH);

  // Turn on AMP
  pinMode(PIN_OUT_AMP_UNMUTE, OUTPUT);
  digitalWrite(PIN_OUT_AMP_UNMUTE, LOW);
  pinMode(PIN_OUT_AMP_ENABLE, OUTPUT);
  digitalWrite(PIN_OUT_AMP_ENABLE, HIGH);

  // Set up car state inputs
  pinMode(PIN_IN_ACC, INPUT);
  pinMode(PIN_IN_LIGHT, INPUT);
  pinMode(PIN_IN_REVERSE, INPUT);

  // Set up SWI inputs
  pinMode(PIN_IN_SWI0, INPUT);
  pinMode(PIN_IN_SWI1, INPUT);

  // Set the PWM pins as output.
  pinMode(PIN_PWM_FAN, OUTPUT);
  analogWrite(PIN_PWM_FAN, 127);
  pinMode(PIN_PWM_BACKLIGHT, OUTPUT);
  analogWriteDiv(PIN_PWM_BACKLIGHT, 255, PWM_PRESCALER, PWM_RESOLUTION);

  pinMode(LED_BUILTIN, OUTPUT);

  load();

  //RawHID.begin(hidData, sizeof(hidData));
  Serial.begin(115200);
  Consumer.begin();

  ack[0] = 0x40;
  ack[1] = 0x40;
}

void button_evt(bool down, uint16_t keycode) {
  // There is a special keycode 0xffff.
  // On UP, triggers SBC hardware reset.

  if (!down && keycode == 0xffff){
    digitalWrite(PIN_OUT_AMP_UNMUTE, LOW);
    digitalWrite(PIN_OUT_AMP_ENABLE, LOW);
    startup_delay = 0;
    delay(250);
    digitalWrite(PIN_RST, HIGH);
    delay(250);
    digitalWrite(PIN_RST, LOW);
    watchdog = 0;
  } else {
    if (down) Consumer.press((ConsumerKeycode)keycode);
    else Consumer.release((ConsumerKeycode)keycode);
  }
}

uint8_t hidcounter = 0;
void loop() {

  // Deal with system power control first. If the ACC line goes off, store the time when
  // we should shut down. When the time is reached, shut down.
  if (digitalRead(PIN_IN_ACC) == 1) {
    accon = true;
    digitalWrite(PIN_OUT_14V, HIGH);
    digitalWrite(PIN_OUT_5V, HIGH);
    if (startup_delay > 2000) // 10 seconds ~ 1000 x 10ms
      digitalWrite(PIN_OUT_AMP_UNMUTE, HIGH);
    else
      startup_delay++;
    digitalWrite(PIN_OUT_AMP_ENABLE, HIGH);
  } else {
    if (accon == true) {
      acccounter = millis() + 30000;
      accon = false;
    } else if (millis() > acccounter) {
      digitalWrite(PIN_OUT_14V, LOW);
      digitalWrite(PIN_OUT_5V, LOW);
    }
    digitalWrite(PIN_OUT_AMP_UNMUTE, LOW);
    digitalWrite(PIN_OUT_AMP_ENABLE, LOW);
  }

  if (prog_mode){
    if (hidcounter == 0){
      // Writes the ADC values to hidraw at ~2Hz
      swi_send[0] = 0x10;
      swi_val[0] = analogRead(PIN_IN_SWI0);
      swi_send[1] = (uint8_t) (swi_val[0] >> 8 & 0xff);
      swi_send[2] = (uint8_t) swi_val[0] & 0xff;
      swi_val[1] = analogRead(PIN_IN_SWI1);
      swi_send[3] = (uint8_t) (swi_val[1] >> 8 & 0xff);
      swi_send[4] = (uint8_t) swi_val[1] & 0xff;
      Serial.write(swi_send, 5);
    }
    hidcounter++;
    if (hidcounter >= 50) hidcounter = 0;
  }

  serAvail = Serial.available();
  if (serAvail){
    hidcmd = Serial.read();
    serAvail--;
    switch(hidcmd){
      case 0x02:
        if (serAvail > 0){
          hidcmd = Serial.read();
          analogWrite(PIN_PWM_FAN, hidcmd);
        }
        break;
      case 0x03:
        if (serAvail > 0){
          hidcmd = Serial.read();
          analogWriteDiv(PIN_PWM_BACKLIGHT, hidcmd, PWM_PRESCALER, PWM_RESOLUTION);
        }
        break;
      case 0x20:
        hidcmd = 0x02;
        Serial.write(&hidcmd, 1);
        break;
      case 0x30:
        // enter programming mode, clear eeprom.
        if (!prog_mode){
          prog_mode = true;
          addy = 0;
          for (uint16_t i = 0; i < EEPROM.length(); i++) {
            EEPROM.write(i, 0);
          }
        }
        Serial.write(ack, 2);
        break;
      case 0x40:
        // if programming mode is active and there are 2 bytes of payload, program the held key with this keycode
        if (prog_mode && serAvail >= 2){
          uint8_t b1 = Serial.read();
          uint8_t b2 = Serial.read();

          serAvail -= 2;
          if (swi_val[0] < SWI_THRESH) {
            EEPROM.write(addy, 'A');
            EEPROM.write(addy + 1, 0);
            EEPROM.write(addy + 2, b1);
            EEPROM.write(addy + 3, b2);
            EEPROM.write(addy + 4, (uint8_t) (swi_val[0] >> 8) & 0xff);
            EEPROM.write(addy + 5, (uint8_t) swi_val[0] & 0xff);
            addy += 6;
          } else if (swi_val[1] < SWI_THRESH) {
            swi_send[2] = 0x1;
            EEPROM.write(addy, 'A');
            EEPROM.write(addy + 1, 1);
            EEPROM.write(addy + 2, b1);
            EEPROM.write(addy + 3, b2);
            EEPROM.write(addy + 4, (uint8_t) (swi_val[1] >> 8) & 0xff);
            EEPROM.write(addy + 5, (uint8_t) swi_val[1] & 0xff);
            addy += 6;
          }
        }
        Serial.write(ack, 2);
        break;
      case 0x41:
        // in prog_mode: skip the ADC read and write the bytes to eeprom
        if (prog_mode){
          EEPROM.write(addy + 0, Serial.read());
          EEPROM.write(addy + 1, Serial.read());
          EEPROM.write(addy + 2, Serial.read());
          EEPROM.write(addy + 3, Serial.read());
          EEPROM.write(addy + 4, Serial.read());
          EEPROM.write(addy + 5, Serial.read());
          uint8_t dbgdt[1];
          dbgdt[0] = 0x99;
          Serial.write(dbgdt, 1);
          addy += 6;
        }
        Serial.write(ack, 2);
        break;
      case 0x4A:
        // dump the EEPROM
        uint8_t eepdata[7];
        eepdata[0] = 0x20;
        for (uint16_t e = 0; e < EEPROM.length(); e += 6) {
          for (uint8_t f = 0; f < 7; f++){
            eepdata[f+1] = EEPROM.read(e + f);
          }
          if (eepdata[1] != 'A') break;
          Serial.write(eepdata, 7);
        }
        Serial.write(ack, 2);
        break;
      case 0x50:
        // save and exit programming mode
        if (prog_mode){
          EEPROM.commit();
          load();
          prog_mode = false;
        }
        Serial.write(ack, 2);
        break;
      case 0x60:
        // abort programming
        prog_mode = false;
        Serial.write(ack, 2);
        break;
      case 0x70:
        // reload key map
        if (!prog_mode) load();
        break;
      case 0x80:
        // watchdog enable / bark
        watchdog = millis();
        Serial.write(ack, 2);
        break;
      case 0x90:
        //watchdog disable
        watchdog = 0;
        Serial.write(ack, 2);
        break;
      case 0xff:
        initiateReset(1);
        tickReset();
    }
  }

  // Third, we check ADCs for button presses, but only if we are NOT in programming mode.
  for (uint8_t i = 0; !prog_mode && i < 2; i++) {
    if (adc[i].entries > 0) {
      uint16_t val = analogRead(i == 0 ? PIN_IN_SWI0 : PIN_IN_SWI1);
      if (abs(adc[i].lastreading - val) <= SWI_HYST){ // make sure we have 2 readings in a row that are consistent
        if (adc[i].down > -1 && (val > SWI_THRESH || abs(adc[i].value[adc[i].down] - val) > SWI_HYST)) { // if button was down and turns off or changes
          button_evt(0, adc[i].keycode[adc[i].down]);
          adc[i].down = -1;
        }
        if (val < SWI_THRESH && adc[i].down < 0) {
          adc[i].down = -1;
          for (uint8_t l = 0; l < adc[i].entries; l++) {
            if (abs(val - adc[i].value[l]) < SWI_HYST) adc[i].down = l;
          }
          if (adc[i].down > -1) button_evt(1, adc[i].keycode[adc[i].down]);
        }
      }
      adc[i].lastreading = val;
    }
  }

  // Fourth, we blink the LED. Note: There is no LED on the board, but it can be helpful to temporarily add one for debugging
  // in order to determine that the microcontroller is alive. Conventional LED can be connected from PA17 to GND. The leads
  // on a conventional LED are long enough to insert it directly. This program will make it blink, the bootloader will make
  // it "breathe".
  loopcounter++;
  if (loopcounter == 190) digitalWrite(LED_BUILTIN, HIGH);
  if (loopcounter >= 200) {
    digitalWrite(LED_BUILTIN, LOW);
    loopcounter = 0;
  }

  // Last, we check the watchdog timer, and reset the CPU if necessary.
  if (watchdog > 0 && watchdog + WATCHDOG_TIMEOUT_MS < millis()){
    digitalWrite(PIN_OUT_AMP_UNMUTE, LOW);
    digitalWrite(PIN_OUT_AMP_ENABLE, LOW);
    startup_delay = 0;
    digitalWrite(PIN_RST, HIGH);
    delay(250);
    digitalWrite(PIN_RST, LOW);
    watchdog = 0;
  }

  delay(10);
}
